

document.addEventListener("DOMContentLoaded", function () {


    var connexion = new MovieDB();

    if(location.pathname.search("fiche-film") > 0){
        let params = (new URL(document.location) ).searchParams;

        connexion.requeteInfoFilm( params.get("id") );

    }
    else{
        connexion.requeteFilmPopulaire();

    }

});


class MovieDB{

    constructor(){
        console.log("Parfait 2");

        this.APIKey = "65b2f853f58981ff3cce60c07900ce25";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "https://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 8;

        this.totalActeur = 6;
    }

    requeteFilmPopulaire(){


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourFilmPopulaire.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();
    }


    retourFilmPopulaire(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            // console.log( data );

            this.afficheFilmPopulaire( data )

        }

    }


    afficheFilmPopulaire( data ){


        for (let i=0; i<this.totalFilm; i++){

            let unArticle = document.querySelector(".template>.film").cloneNode(true);

            unArticle.querySelector("h1").innerText = data[i].title;

            if(data[i].overview === ""){

                unArticle.querySelector("p.description").innerText = "This shit empty boi"

            } else{


                unArticle.querySelector("p.description").innerText = data[i].overview;

            }

            unArticle.querySelector("a").setAttribute("href","fiche-film.html?id=" + data[i].id);

            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w780" + data[i].poster_path);


            document.querySelector(".liste-films").appendChild(unArticle);

            //console.log(data[0].title);
            //console.log(data[0].vote_count);

        }


    }








    requeteInfoFilm(movieId){

        console.log("Test " + movieId);




        let xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourInfoFilm.bind(this));

        // https://api.themoviedb.org/3/movie/%7Bmovie_id%7D?language=en-US&api_key=%3C%3Capi_key%3E%3E
        xhr.open("GET", this.baseURL+"movie/"+ movieId +"?language="+ this.lang +"&api_key="+ this.APIKey);

        xhr.send();


    }

    retourInfoFilm(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            console.log( data );

            this.afficheInfoFilm( data )

        }

    }


    afficheInfoFilm( data ){



            let unArticle = document.querySelector(".fiche-film");

            unArticle.querySelector("h1").innerText = data.title;

            if(data.overview === ""){

                unArticle.querySelector("p.description").innerText = "This shit empty boi"

            } else{


                unArticle.querySelector("p.description").innerText = data.overview;

            }

            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w780" + data.poster_path);

            this.requeteActeur(data.id);



    }

    requeteActeur(movieID){


        let xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourActeur.bind(this));
        // https://api.themoviedb.org/3/movie/%7Bmovie_id%7D/credits?api_key=%3C%3Capi_key%3E%3E
        xhr.open("GET", this.baseURL+"movie/"+ movieID +"/credits?api_key="+ this.APIKey);

        xhr.send();


    }

    retourActeur(e){

        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            console.log( data );

            this.afficheActeur( data )

        }

    }


    afficheActeur( data ){



        let unArticle = document.querySelector(".templateActeur>.liste-acteurs").cloneNode(true);

        unArticle.querySelector(".liste-acteurs").innerText = data.name;

        // this.requeteActeur(data.id);



    }








}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzY3JpcHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG5cbmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJET01Db250ZW50TG9hZGVkXCIsIGZ1bmN0aW9uICgpIHtcblxuXG4gICAgdmFyIGNvbm5leGlvbiA9IG5ldyBNb3ZpZURCKCk7XG5cbiAgICBpZihsb2NhdGlvbi5wYXRobmFtZS5zZWFyY2goXCJmaWNoZS1maWxtXCIpID4gMCl7XG4gICAgICAgIGxldCBwYXJhbXMgPSAobmV3IFVSTChkb2N1bWVudC5sb2NhdGlvbikgKS5zZWFyY2hQYXJhbXM7XG5cbiAgICAgICAgY29ubmV4aW9uLnJlcXVldGVJbmZvRmlsbSggcGFyYW1zLmdldChcImlkXCIpICk7XG5cbiAgICB9XG4gICAgZWxzZXtcbiAgICAgICAgY29ubmV4aW9uLnJlcXVldGVGaWxtUG9wdWxhaXJlKCk7XG5cbiAgICB9XG5cbn0pO1xuXG5cbmNsYXNzIE1vdmllREJ7XG5cbiAgICBjb25zdHJ1Y3Rvcigpe1xuICAgICAgICBjb25zb2xlLmxvZyhcIlBhcmZhaXQgMlwiKTtcblxuICAgICAgICB0aGlzLkFQSUtleSA9IFwiNjViMmY4NTNmNTg5ODFmZjNjY2U2MGMwNzkwMGNlMjVcIjtcblxuICAgICAgICB0aGlzLmxhbmcgPSBcImZyLUNBXCI7XG5cbiAgICAgICAgdGhpcy5iYXNlVVJMID0gXCJodHRwczovL2FwaS50aGVtb3ZpZWRiLm9yZy8zL1wiO1xuXG4gICAgICAgIHRoaXMuaW1nUGF0aCA9IFwiaHR0cHM6Ly9pbWFnZS50bWRiLm9yZy90L3AvXCI7XG5cbiAgICAgICAgdGhpcy5sYXJnZXVyQWZmaWNoZSA9IFtcIjkyXCIsIFwiMTU0XCIsIFwiMTg1XCIsIFwiMzQyXCIsIFwiNTAwXCIsIFwiNzgwXCJdO1xuXG4gICAgICAgIHRoaXMubGFyZ2V1clRldGVBZmZpY2hlID0gW1wiNDVcIiwgXCIxODVcIl07XG5cbiAgICAgICAgdGhpcy50b3RhbEZpbG0gPSA4O1xuXG4gICAgICAgIHRoaXMudG90YWxBY3RldXIgPSA2O1xuICAgIH1cblxuICAgIHJlcXVldGVGaWxtUG9wdWxhaXJlKCl7XG5cblxuICAgICAgICB2YXIgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG5cblxuICAgICAgICB4aHIuYWRkRXZlbnRMaXN0ZW5lcihcInJlYWR5c3RhdGVjaGFuZ2VcIiwgdGhpcy5yZXRvdXJGaWxtUG9wdWxhaXJlLmJpbmQodGhpcykpO1xuXG4gICAgICAgIC8veGhyLm9wZW4oXCJHRVRcIiwgXCJodHRwczovL2FwaS50aGVtb3ZpZWRiLm9yZy8zL21vdmllL3BvcHVsYXI/cGFnZT0xJmxhbmd1YWdlPWVuLVVTJmFwaV9rZXk9JTNDJTNDYXBpX2tleSUzRSUzRVwiKTtcbiAgICAgICAgeGhyLm9wZW4oXCJHRVRcIiwgdGhpcy5iYXNlVVJMICsgXCJtb3ZpZS9wb3B1bGFyP3BhZ2U9MSZsYW5ndWFnZT1cIiArIHRoaXMubGFuZyArIFwiJmFwaV9rZXk9XCIgKyB0aGlzLkFQSUtleSk7XG5cbiAgICAgICAgeGhyLnNlbmQoKTtcbiAgICB9XG5cblxuICAgIHJldG91ckZpbG1Qb3B1bGFpcmUoZSl7XG5cbiAgICAgICAgdmFyIHRhcmdldCA9IGUuY3VycmVudFRhcmdldDtcblxuICAgICAgICB2YXIgZGF0YTtcblxuICAgICAgICBpZiAodGFyZ2V0LnJlYWR5U3RhdGUgPT09IHRhcmdldC5ET05FKSB7XG5cbiAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKHRhcmdldC5yZXNwb25zZVRleHQpLnJlc3VsdHM7XG5cbiAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKCBkYXRhICk7XG5cbiAgICAgICAgICAgIHRoaXMuYWZmaWNoZUZpbG1Qb3B1bGFpcmUoIGRhdGEgKVxuXG4gICAgICAgIH1cblxuICAgIH1cblxuXG4gICAgYWZmaWNoZUZpbG1Qb3B1bGFpcmUoIGRhdGEgKXtcblxuXG4gICAgICAgIGZvciAobGV0IGk9MDsgaTx0aGlzLnRvdGFsRmlsbTsgaSsrKXtcblxuICAgICAgICAgICAgbGV0IHVuQXJ0aWNsZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIudGVtcGxhdGU+LmZpbG1cIikuY2xvbmVOb2RlKHRydWUpO1xuXG4gICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcImgxXCIpLmlubmVyVGV4dCA9IGRhdGFbaV0udGl0bGU7XG5cbiAgICAgICAgICAgIGlmKGRhdGFbaV0ub3ZlcnZpZXcgPT09IFwiXCIpe1xuXG4gICAgICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJwLmRlc2NyaXB0aW9uXCIpLmlubmVyVGV4dCA9IFwiVGhpcyBzaGl0IGVtcHR5IGJvaVwiXG5cbiAgICAgICAgICAgIH0gZWxzZXtcblxuXG4gICAgICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJwLmRlc2NyaXB0aW9uXCIpLmlubmVyVGV4dCA9IGRhdGFbaV0ub3ZlcnZpZXc7XG5cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJhXCIpLnNldEF0dHJpYnV0ZShcImhyZWZcIixcImZpY2hlLWZpbG0uaHRtbD9pZD1cIiArIGRhdGFbaV0uaWQpO1xuXG4gICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcImltZ1wiKS5zZXRBdHRyaWJ1dGUoXCJzcmNcIiwgdGhpcy5pbWdQYXRoICsgXCJ3NzgwXCIgKyBkYXRhW2ldLnBvc3Rlcl9wYXRoKTtcblxuXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmxpc3RlLWZpbG1zXCIpLmFwcGVuZENoaWxkKHVuQXJ0aWNsZSk7XG5cbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coZGF0YVswXS50aXRsZSk7XG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKGRhdGFbMF0udm90ZV9jb3VudCk7XG5cbiAgICAgICAgfVxuXG5cbiAgICB9XG5cblxuXG5cblxuXG5cblxuICAgIHJlcXVldGVJbmZvRmlsbShtb3ZpZUlkKXtcblxuICAgICAgICBjb25zb2xlLmxvZyhcIlRlc3QgXCIgKyBtb3ZpZUlkKTtcblxuXG5cblxuICAgICAgICBsZXQgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG5cbiAgICAgICAgeGhyLmFkZEV2ZW50TGlzdGVuZXIoXCJyZWFkeXN0YXRlY2hhbmdlXCIsIHRoaXMucmV0b3VySW5mb0ZpbG0uYmluZCh0aGlzKSk7XG5cbiAgICAgICAgLy8gaHR0cHM6Ly9hcGkudGhlbW92aWVkYi5vcmcvMy9tb3ZpZS8lN0Jtb3ZpZV9pZCU3RD9sYW5ndWFnZT1lbi1VUyZhcGlfa2V5PSUzQyUzQ2FwaV9rZXklM0UlM0VcbiAgICAgICAgeGhyLm9wZW4oXCJHRVRcIiwgdGhpcy5iYXNlVVJMK1wibW92aWUvXCIrIG1vdmllSWQgK1wiP2xhbmd1YWdlPVwiKyB0aGlzLmxhbmcgK1wiJmFwaV9rZXk9XCIrIHRoaXMuQVBJS2V5KTtcblxuICAgICAgICB4aHIuc2VuZCgpO1xuXG5cbiAgICB9XG5cbiAgICByZXRvdXJJbmZvRmlsbShlKXtcblxuICAgICAgICB2YXIgdGFyZ2V0ID0gZS5jdXJyZW50VGFyZ2V0O1xuXG4gICAgICAgIHZhciBkYXRhO1xuXG4gICAgICAgIGlmICh0YXJnZXQucmVhZHlTdGF0ZSA9PT0gdGFyZ2V0LkRPTkUpIHtcblxuICAgICAgICAgICAgZGF0YSA9IEpTT04ucGFyc2UodGFyZ2V0LnJlc3BvbnNlVGV4dCk7XG5cbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCBkYXRhICk7XG5cbiAgICAgICAgICAgIHRoaXMuYWZmaWNoZUluZm9GaWxtKCBkYXRhIClcblxuICAgICAgICB9XG5cbiAgICB9XG5cblxuICAgIGFmZmljaGVJbmZvRmlsbSggZGF0YSApe1xuXG5cblxuICAgICAgICAgICAgbGV0IHVuQXJ0aWNsZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuZmljaGUtZmlsbVwiKTtcblxuICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJoMVwiKS5pbm5lclRleHQgPSBkYXRhLnRpdGxlO1xuXG4gICAgICAgICAgICBpZihkYXRhLm92ZXJ2aWV3ID09PSBcIlwiKXtcblxuICAgICAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwicC5kZXNjcmlwdGlvblwiKS5pbm5lclRleHQgPSBcIlRoaXMgc2hpdCBlbXB0eSBib2lcIlxuXG4gICAgICAgICAgICB9IGVsc2V7XG5cblxuICAgICAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwicC5kZXNjcmlwdGlvblwiKS5pbm5lclRleHQgPSBkYXRhLm92ZXJ2aWV3O1xuXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiaW1nXCIpLnNldEF0dHJpYnV0ZShcInNyY1wiLCB0aGlzLmltZ1BhdGggKyBcInc3ODBcIiArIGRhdGEucG9zdGVyX3BhdGgpO1xuXG4gICAgICAgICAgICB0aGlzLnJlcXVldGVBY3RldXIoZGF0YS5pZCk7XG5cblxuXG4gICAgfVxuXG4gICAgcmVxdWV0ZUFjdGV1cihtb3ZpZUlEKXtcblxuXG4gICAgICAgIGxldCB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcblxuXG4gICAgICAgIHhoci5hZGRFdmVudExpc3RlbmVyKFwicmVhZHlzdGF0ZWNoYW5nZVwiLCB0aGlzLnJldG91ckFjdGV1ci5iaW5kKHRoaXMpKTtcbiAgICAgICAgLy8gaHR0cHM6Ly9hcGkudGhlbW92aWVkYi5vcmcvMy9tb3ZpZS8lN0Jtb3ZpZV9pZCU3RC9jcmVkaXRzP2FwaV9rZXk9JTNDJTNDYXBpX2tleSUzRSUzRVxuICAgICAgICB4aHIub3BlbihcIkdFVFwiLCB0aGlzLmJhc2VVUkwrXCJtb3ZpZS9cIisgbW92aWVJRCArXCIvY3JlZGl0cz9hcGlfa2V5PVwiKyB0aGlzLkFQSUtleSk7XG5cbiAgICAgICAgeGhyLnNlbmQoKTtcblxuXG4gICAgfVxuXG4gICAgcmV0b3VyQWN0ZXVyKGUpe1xuXG4gICAgICAgIGxldCB0YXJnZXQgPSBlLmN1cnJlbnRUYXJnZXQ7XG5cbiAgICAgICAgbGV0IGRhdGE7XG5cbiAgICAgICAgaWYgKHRhcmdldC5yZWFkeVN0YXRlID09PSB0YXJnZXQuRE9ORSkge1xuXG4gICAgICAgICAgICBkYXRhID0gSlNPTi5wYXJzZSh0YXJnZXQucmVzcG9uc2VUZXh0KTtcblxuICAgICAgICAgICAgY29uc29sZS5sb2coIGRhdGEgKTtcblxuICAgICAgICAgICAgdGhpcy5hZmZpY2hlQWN0ZXVyKCBkYXRhIClcblxuICAgICAgICB9XG5cbiAgICB9XG5cblxuICAgIGFmZmljaGVBY3RldXIoIGRhdGEgKXtcblxuXG5cbiAgICAgICAgbGV0IHVuQXJ0aWNsZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIudGVtcGxhdGVBY3RldXI+Lmxpc3RlLWFjdGV1cnNcIikuY2xvbmVOb2RlKHRydWUpO1xuXG4gICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiLmxpc3RlLWFjdGV1cnNcIikuaW5uZXJUZXh0ID0gZGF0YS5uYW1lO1xuXG4gICAgICAgIC8vIHRoaXMucmVxdWV0ZUFjdGV1cihkYXRhLmlkKTtcblxuXG5cbiAgICB9XG5cblxuXG5cblxuXG5cblxufSJdLCJmaWxlIjoic2NyaXB0LmpzIn0=
