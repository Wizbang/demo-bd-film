

document.addEventListener("DOMContentLoaded", function () {


    var connexion = new MovieDB();

    if(location.pathname.search("fiche-film") > 0){
        let params = (new URL(document.location) ).searchParams;

        connexion.requeteInfoFilm( params.get("id") );

    }
    else{
        connexion.requeteFilmPopulaire();

    }

});


class MovieDB{

    constructor(){
        console.log("Parfait 2");

        this.APIKey = "65b2f853f58981ff3cce60c07900ce25";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "https://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 8;

        this.totalActeur = 6;
    }

    requeteFilmPopulaire(){


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourFilmPopulaire.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();
    }


    retourFilmPopulaire(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            // console.log( data );

            this.afficheFilmPopulaire( data )

        }

    }


    afficheFilmPopulaire( data ){


        for (let i=0; i<this.totalFilm; i++){

            let unArticle = document.querySelector(".template>.film").cloneNode(true);

            unArticle.querySelector("h1").innerText = data[i].title;

            if(data[i].overview === ""){

                unArticle.querySelector("p.description").innerText = "This shit empty boi"

            } else{


                unArticle.querySelector("p.description").innerText = data[i].overview;

            }

            unArticle.querySelector("a").setAttribute("href","fiche-film.html?id=" + data[i].id);

            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w780" + data[i].poster_path);


            document.querySelector(".liste-films").appendChild(unArticle);

            //console.log(data[0].title);
            //console.log(data[0].vote_count);

        }


    }








    requeteInfoFilm(movieId){

        console.log("Test " + movieId);




        let xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourInfoFilm.bind(this));

        // https://api.themoviedb.org/3/movie/%7Bmovie_id%7D?language=en-US&api_key=%3C%3Capi_key%3E%3E
        xhr.open("GET", this.baseURL+"movie/"+ movieId +"?language="+ this.lang +"&api_key="+ this.APIKey);

        xhr.send();


    }

    retourInfoFilm(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            console.log( data );

            this.afficheInfoFilm( data )

        }

    }


    afficheInfoFilm( data ){



            let unArticle = document.querySelector(".fiche-film");

            unArticle.querySelector("h1").innerText = data.title;

            if(data.overview === ""){

                unArticle.querySelector("p.description").innerText = "This shit empty boi"

            } else{


                unArticle.querySelector("p.description").innerText = data.overview;

            }

            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w780" + data.poster_path);

            this.requeteActeur(data.id);



    }

    requeteActeur(movieID){


        let xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourActeur.bind(this));
        // https://api.themoviedb.org/3/movie/%7Bmovie_id%7D/credits?api_key=%3C%3Capi_key%3E%3E
        xhr.open("GET", this.baseURL+"movie/"+ movieID +"/credits?api_key="+ this.APIKey);

        xhr.send();


    }

    retourActeur(e){

        let target = e.currentTarget;

        let data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            console.log( data );

            this.afficheActeur( data )

        }

    }


    afficheActeur( data ){



        let unArticle = document.querySelector(".templateActeur>.liste-acteurs").cloneNode(true);

        unArticle.querySelector(".liste-acteurs").innerText = data.name;

        // this.requeteActeur(data.id);



    }








}